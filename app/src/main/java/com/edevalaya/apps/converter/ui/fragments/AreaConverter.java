package com.edevalaya.apps.converter.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.edevalaya.apps.converter.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AreaConverter extends Fragment {

    Spinner spin_types;
    EditText input_value;
    TextView result;

    public AreaConverter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView =  inflater.inflate(R.layout.fragment_area_converter, container, false);

        spin_types = (Spinner) rootView.findViewById(R.id.input_type);
        input_value = (EditText) rootView.findViewById(R.id.input_value);
        result = (TextView) rootView.findViewById(R.id.result);

        ArrayList<String> types = new ArrayList<String>();
        types.add("sq. ft");
        types.add("sq. inch");
        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_dropdown_item,types);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_types.setAdapter(typeAdapter);

        input_value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(input_value.getText().toString().toString().trim().length() > 0) {
                    ArrayList<String> value = converter(input_value.getText().toString());
                    String reslt = "<b>Ropani: </b>" + value.get(0) + "<br/><b> Ana: </b>" + value.get(1) + "<br/><b> Paisa: </b>" + value.get(2) + "<br/><b> Daam: </b>" + value.get(3);
                    result.setText(Html.fromHtml(reslt));
                }
                else{
                    result.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return rootView;
    }

    public ArrayList<String> converter(String input)
    {
        ArrayList<String> result = new ArrayList<String>();
        Float totalArea = Float.parseFloat(input);
        Float conversion = (totalArea/5476);
        Double ropani = Math.floor(conversion);
        result.add(0,String.valueOf(ropani));
        Double paisaAna = (conversion - ropani)*16;
        Double ana = Math.floor(paisaAna);
        result.add(1,String.valueOf(ana));
        Double paisaDam = (paisaAna - ana)*4;
        Double paisa = Math.floor(paisaDam);
        result.add(2,String.valueOf(paisa));
        Double daam = (paisaDam - paisa)*4;
        result.add(3,String.format("%.3f",daam));
        return result;
    }

}
